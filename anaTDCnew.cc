#include <iostream> 
#include <iomanip>
#include <fstream>
#include <istream>
#include <sstream>
#include <string>
#include <bitset>


void rootSetup(){

  gStyle->SetOptFit(1111);
  gStyle->SetOptStat(11111111);
  gStyle->SetStatY(0.92);                
  gStyle->SetStatX(0.95);                
  gStyle->SetStatW(0.18);               
  gStyle->SetStatH(0.18); 
 
  //for x-axis
  gStyle->SetLabelSize(0.04,"X");
  gStyle->SetTitleSize(0.04,"X");
  gStyle->SetTitleOffset(1,"X");
  
  //for y-axis
  gStyle->SetLabelSize(0.04,"Y");
  gStyle->SetTitleSize(0.04,"Y");                                      
  gStyle->SetTitleOffset(1.1,"Y");
  gStyle->SetName("");
  gStyle->SetTitle("");

  //set Canvas
  gStyle->SetFillColor(0);
  gStyle->SetCanvasColor(0); 
  gStyle->SetPadColor(0); 
  gStyle->SetFrameFillColor(0); 
  gStyle->SetHistFillColor(0); 
  gStyle->SetTitleFillColor(0); 
  gStyle->SetFillColor(0);

  //set title
  gStyle->SetTitleX(0.5);
  gStyle->SetTitleAlign(23); 
  gStyle->SetTitleBorderSize(0); 
  gStyle->SetTitleFont(40);
  gStyle->SetTitleFontSize(0.05);

}

int ana_SLink_header1(int &slink_header1,int &err,int &evt_type,int &srcID,int &evt_size){

  //S-Link header1 : error(1) evt_type(5) srcID(10) evt_size(16)
  err      = ( slink_header1 & 0x80000000 ) >> 31;        
  evt_type = ( slink_header1 & 0x0f800000 ) >> 26;
  srcID    = ( slink_header1 & 0x03ff0000 ) >> 16;
  evt_size = ( slink_header1 & 0x0000ffff ) >> 0 ;

}

int ana_SLink_header2(int &slink_header2,int &stat,int &spill_num,int &evt_num){

  //S-Link header2 : stat(1) spill_no(11) evt_num(20)  
  stat      = ( slink_header2 & 0x80000000 ) >> 31;
  spill_num = ( slink_header2 & 0x07ff0000 ) >> 20;
  evt_num   = ( slink_header2 & 0x000fffff ) >> 0 ;
  
}

int ana_SLink_header3(int &slink_header3,int &format,int &err_word,int &tcs_err, int &status){
  //S-Link header3 : format(8) err_word(8) tcs_err(8) status(8)
  format   = ( slink_header3 & 0xff000000 ) >> 24;
  err_word = ( slink_header3 & 0x00ff0000 ) >> 16; 
  tcs_err  = ( slink_header3 & 0x0000ff00 ) >> 8 ; 
  status   = ( slink_header3 & 0x000000ff ) >> 0 ;
}


int ana_DC5_header(int &word,int &idle_0x00,int &TBO,int &fem_evt_num,int &trig_time_10us,int &fem_id,int &idle_0x05,int &Temp_7To4){

  //DC5 header   : idle_0x00(1) TBO(1) fem_evt_num(6) trig_time_10us(11) fem_id(5) idle_0x05(4) Temp_7To4(4)
  idle_0x00      = (word & 0x80000000) >> 31;
  TBO            = (word & 0x40000000) >> 30;
  fem_evt_num    = (word & 0x3f000000) >> 24;
  trig_time_10us = (word & 0x00fe0000) >> 13;
  fem_id         = (word & 0x00001f00) >>  8; 
  idle_0x05      = (word & 0x000000f0) >>  4;
  Temp_7To4      = (word & 0x0000000f) >>  0;
  
}

int ana_DC5_tdc(int &word,int &idle_0xa5,int &rwOverlap,int &idle_0x00,int &maxLimt,int &hit_timing,int &fem_ch,int &fem_mode){
  
  //DC5 data     : idle_0xa5(8) rwOverlap(1) idle_0x00(1) maxLimt(1) hit_timing(13) fem_ch(4) fem_mode(4)
  idle_0xa5      = (word & 0xff000000) >> 24;  
  rwOverlap      = (word & 0x00800000) >> 23;
  idle_0x00      = (word & 0x00400000) >> 22;
  maxLimt        = (word & 0x00200000) >> 21;
  hit_timing     = (word & 0x001fff00) >>  8;    
  fem_ch         = (word & 0x000000f0) >>  4;
  fem_mode       = (word & 0x0000000f) >>  0;  
}

int ana_DC5_footnote(int &word,int &idle_0x5a,int &rwOverlap,int &idle_0x00,int &maxLimt,int &trig_time_1ns,int &idle_0x0a,int &Temp_3To9){

  //DC5 footnote : idle_0x5a(8) rwOverlap(1) idle_0x00(1) maxLimt(1) trig_time_1ns(13) idle_0x0a(4) Temp_3To9(4)
  idle_0x5a      = (word & 0xff000000) >> 24;  
  rwOverlap      = (word & 0x00800000) >> 23;
  idle_0x00      = (word & 0x00400000) >> 22;
  maxLimt        = (word & 0x00200000) >> 21; 
  trig_time_1ns  = (word & 0x001fff00) >>  8;
  idle_0x0a      = (word & 0x000000f0) >>  4;
  Temp_3To9      = (word & 0x0000000f) >>  0;
}


//void anaTDCnew(char *dir,int thres,const int numFEM){
void anaTDCnew(){
  char dir[100] ="/afs/cern.ch/user/c/chsieh/DC05/newFEE/dc05Decoding";
  int thres        = 650;
  const int numFEM =   1;
  
  //========== root setup ============
  rootSetup();

  //========== variables =============
 
  const int numSLinkHeader =  4+2; //0x00000000 + GANDALF*2 lines + DCM*3 lines  
  const int numSLinkTrailer=  1;
  const int numFemHeader   =  1;
  const int numFEMCh       =  16;  
  const int numFemTrailer  =  1;

  int maxWordTmp = 1000*(70*numFEM); 
  const int maxWord = maxWordTmp; 
  int       iword; 
  string    wordStr[maxWord];
  long long int  wordInt[maxWord];
  char      bitChar[maxWord][8];
  int       bitInt[maxWord][8];
  int       numWord;
  const int maxHit=32;
  

  //GANDALF data package(NO NEED TO DECODE IT)
  //int slink_header1; int err,evt_type,srcID,evt_size;  
  //int slink_header2; int stat,spill_num,evt_num;       

  //DCM data package
  int slink_header1; int err,evt_type,srcID,evt_size;
  int slink_header2; int stat,spill_num,evt_num;
  int slink_header3; int format,err_word,tcs_err,status; 

  //FEM data package
  int dc5_header;   int dc5_header_idle_0x00,TBO,fem_evt_num,trig_time_10us,fem_id,idle_0x05,Temp_7To4;
  int dc5_tdc;      int idle_0xa5,dc5_tdc_rwOverlap,dc5_tdc_idle_0x00,dc5_tdc_maxLimt,hit_timing,fem_ch,fem_mode;
  int dc5_footnote; int idle_0x5a,dc5_footnote_rwOverlap,dc5_footnote_idle_0x00,dc5_footnote_maxLimt,trig_time_1ns,idle_0x0a,Temp_3To9;
  
  //========== histogram and plot.root =======================

  TH1D*tdc_ch[numFEM][numFEMCh];
  TH1D*tdc_chDiff[numFEM][numFEMCh];
  TH1D*tdc_trig[numFEM];
  TH1D*tdc_trigDiff[numFEM];
  TH1D*tdc_ch_trig[numFEM][numFEMCh];

  TH1D*phase_trig[numFEM];
  TH1D*phase_ch[numFEM][numFEMCh];

  
  for(int iFEM=0;iFEM<numFEM;iFEM++){
    tdc_trig[iFEM] = new TH1D(Form("FEM%02d_trig",iFEM),Form("FEM%02d : trig",iFEM),10000,0,10000); 
    tdc_trigDiff[iFEM] = new TH1D(Form("FEM%02d_trigDiff",iFEM),Form("FEM%02d : trigDiff",iFEM),10000,0,10000); 
    phase_trig[iFEM] = new TH1D(Form("FEM%02d_trigPhase",iFEM),Form("FEM%02d : trigPhase",iFEM),5,0,5);
    for(int iCh=0; iCh<numFEMCh; iCh++){ 
      tdc_ch[iFEM][iCh] = new TH1D(Form("FEM%02d_Ch%02d",iFEM,iCh),Form("FEM%02d_Ch%02d : hit ",iFEM,iCh),10000,0,10000); 
      tdc_ch_trig[iFEM][iCh] = new TH1D(Form("FEM%02d_Ch%02d",iFEM,iCh),Form("FEM%02d_Ch%02d : hit-trig ",iFEM,iCh),20000,-10000,10000); 
      tdc_chDiff[iFEM][iCh] = new TH1D(Form("FEM%02d_Ch%02d",iFEM,iCh),Form("FEM%02d_Ch%02d : chHitDiff ",iFEM,iCh),1000,-500,500); 
      phase_ch[iFEM][iCh] = new TH1D(Form("FEM%02d_Ch%02d",iFEM,iCh),Form("FEM%02d_Ch%02d : phase ",iFEM,iCh),5,0,5); 
    }  
  }

  //========== open input:thre.txt & output:thes_ana.txt file ============= 
  char file[120];
  sprintf(file,"%s/%i.txt",dir,thres);
  ifstream indata(file);  

  //=============read data================

  iword = 0;
  while(indata>>wordStr[iword]){
    
    //printf("\n%5d %10s",iword,wordStr[iword].c_str());
    
    strcpy(bitChar[iword],wordStr[iword].c_str()); 
    for(int i=0; i<8; i++){
      stringstream ss;
      ss << hex << bitChar[iword][i];  
      ss >> bitInt[iword][i];
      //printf(" %02d",bitInt[iword][i]);
    }
    wordInt[iword] = bitInt[iword][0]*pow(16,7) + bitInt[iword][1]*pow(16,6) + bitInt[iword][2]*pow(16,5) + bitInt[iword][3]*pow(16,4) + bitInt[iword][4]*pow(16,3) + bitInt[iword][5]*pow(16,2) + bitInt[iword][6]*pow(16,1) + bitInt[iword][7]*pow(16,0);
    //printf("\n%5d 0x%08x",iword,wordInt[iword]);
    iword++;
  } 
  indata.close();
  numWord = iword; 
  
  

  
  //===========ana data=================
  bool if_header = false;
  bool error_data_complete = false;
  bool error_fem_evt_num   = false; 
  int fem_evt_num_old = -1; 


  int lineFirst = -1; int lineEnd=-1;
  int femFirst=-1 ; int femEnd=-1;
  int trig10usOld = -1; int trigDiffTmp = -1;
  
  int chHitOld[numFEMCh],chHit[numFEMCh]; int chHitDiff = -1;
  memset(chHit,-1,sizeof(chHit)); 
  memset(chHitOld,-1,sizeof(chHitOld));

  int phase;

  //numWord=1000;
    
  for(int i=0; i<numWord; i++){

    
    if(wordStr[i]=="00000000") { lineFirst = i; }
    else if(wordStr[i]=="cfed1200") 
      {
	lineEnd = i;

	for(int j=lineFirst; j<=lineEnd; j++) 
	  {
	  
	    
	    if(j==lineFirst+0) printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
	    else if(j==lineFirst+1) 
	      { 
		//decode GANDALF_SLink_Header1
	    	printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
	    	ana_SLink_header1(wordInt[j],err,evt_type,srcID,evt_size);
	    	printf("%25s","GANDALF_SLink_Header1"); 
	    	printf("  Error: %d,  Evt_Type: 0x%x,  SrcID: %d,  Evt_Size: %d ",err,evt_type,srcID,evt_size);
	      }
	    else if(j==lineFirst+2)
	      {
		//decode GANDALF_SLink_Header2
	    	printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
	    	ana_SLink_header2(wordInt[j],stat,spill_num,evt_num);
	    	printf("%25s","GANDALF_SLink_Header2");
	    	printf("  Stat: %d,  Spill_Num: %d,  Evt_Num: %d",stat,spill_num,evt_num);
	      }
	    else if(j==lineFirst+3) 
	      { 
		//decode DCM_SLink_Header1
	    	printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
	    	ana_SLink_header1(wordInt[j],err,evt_type,srcID,evt_size);
	    	printf("%25s","DCM_SLink_Header1"); 
	    	printf("  Error: %d,  Evt_Type: 0x%x,  SrcID: %d,  Evt_Size: %d ",err,evt_type,srcID,evt_size);
	      }
	    else if(j==lineFirst+4)
	      {
		//decode DCM_SLink_Header2
	    	printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
	    	ana_SLink_header2(wordInt[j],stat,spill_num,evt_num);
	    	printf("%25s","DCM_SLink_Header2");
	    	printf("  Stat: %d,  Spill_Num: %d,  Evt_Num: %d",stat,spill_num,evt_num);
	      }
	    else if(j==lineFirst+5)
	      { 
		//decode DCM_SLink_Header3
	    	printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
	    	ana_SLink_header3(wordInt[j],format,err_word,tcs_err,status);
	    	printf("%25s","DCM_SLink_Header3");
	    	printf("  Format: %d, #Err: %d, TCS_Err: 0x%02x, Status: 0x%02x",format,err_word,tcs_err,status);
	      }
	    else if(j==lineEnd) printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
	    else  
	      {
	       
	
		if( (wordInt[j]>>31)==0 && (wordInt[j]>>24)!=0x5a )
		  {
		    //decode femHeader
		    printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
		    ana_DC5_header(wordInt[j],dc5_header_idle_0x00,TBO,fem_evt_num,trig_time_10us,fem_id,idle_0x05,Temp_7To4);
		    printf("%25s","FEM_Header");
		    printf("  TBO: %1d, FEM_ID: %2d,  FEM_evt#: %2d, Trig_10us: %5d, Temp_7To4: %3d",TBO,fem_id,fem_evt_num,trig_time_10us,Temp_7To4);

		    //tdc_trigDiff
		    if(trig10usOld!=-1 && trig_time_10us>trig10usOld) trigDiffTmp = trig_time_10us-trig10usOld;
		    else if (trig10usOld!=-1 && trig_time_10us<trig10usOld) trigDiffTmp = trig_time_10us+(0x7ff-trig10usOld);
		    else ;
		    
		    if(trig10usOld!=-1){ printf(",  trigDiff_10us: %5d",trigDiffTmp); tdc_trigDiff[fem_id]->Fill(trigDiffTmp); }
		    trig10usOld = trig_time_10us;	

		    //error flag:fem_evt_num
		    if(fem_evt_num_old!=-1 && fem_evt_num_old!=fem_evt_num-1) 
		      { 
			error_data_complete = true;  
			printf("\nError_ : fem_evt_num_old != fem_evt_num, FEM lose data!");
			fem_evt_num_old = fem_evt_num;
		      }

		    
		    femFirst=j;
		    memset(chHit,-1,sizeof(chHit)); 
		    memset(chHitOld,-1,sizeof(chHitOld));

	    
		  }
		else if ( (wordInt[j]>>31)==0 && (wordInt[j]>>24)==0x5a )
		  {
		    //decode femFootnote
		    printf("\n%5d %s",j-lineFirst,wordStr[j].c_str());
		    ana_DC5_footnote(wordInt[j],idle_0x5a,dc5_footnote_rwOverlap,dc5_footnote_idle_0x00,dc5_footnote_maxLimt,trig_time_1ns,idle_0x0a,Temp_3To9);
		    printf("%25s","FEM_Trailer"); 
		    printf("  Trig  : %2d, trigTime1ns: %5d, RW: %1d, MaxLimt: %1d, Temp3To9: %2d",0,trig_time_1ns,dc5_footnote_rwOverlap,dc5_footnote_maxLimt,Temp_3To9);
		    
		    //tdc_trig
		    tdc_trig[fem_id]->Fill(trig_time_1ns);    

		    //tdc phase = Bit9Bit8....Bit7-Bit0
		    phase = (wordInt[j] &  0x3ff)>>8;
		    printf(", phase: %2d",phase);
		    phase_trig[fem_id]->Fill(phase);

		    //error flag
		    if(dc5_footnote_rwOverlap!=0) printf("\nError_ : dc5_footnote_rwOverlap!=0!");
		    if(dc5_footnote_maxLimt!=0)   printf("\nError_ : dc5_footnote_maxLimt!=0!");
		 
		    		    
		    femEnd=j;

		    for(int ihit=femFirst+1; ihit<femEnd; ihit++) 
		     {
		       //decode femData
		    	printf("\n%5d %s",ihit-lineFirst,wordStr[ihit].c_str());
		    	ana_DC5_tdc(wordInt[ihit],idle_0xa5,dc5_tdc_rwOverlap,dc5_tdc_idle_0x00,dc5_tdc_maxLimt,hit_timing,fem_ch,fem_mode);
		    	printf("%23s%02d","FEM_data#",ihit-femFirst);
		    	printf("  FEM_ch: %2d, hitTime_1ns: %5d, RW: %1d, MaxLimt: %1d, FEM_mode: %2d",fem_ch,hit_timing,dc5_tdc_rwOverlap,dc5_tdc_maxLimt,fem_mode);
		    	
			//tdc_ch & tdc_ch_trig
		    	tdc_ch[fem_id][fem_ch]->Fill(hit_timing);
		    	tdc_ch_trig[fem_id][fem_ch]->Fill(hit_timing-trig_time_1ns); 
			printf(", hit-trig: %5d",hit_timing-trig_time_1ns); 

			//tdc phase = Bit9Bit8....Bit7-Bit0
			phase = (wordInt[ihit] &  0x3ff)>>8;
			printf(", phase: %2d",phase);
			phase_ch[fem_id][fem_ch]->Fill(phase);

			//tdc_chHitDiff
		    	chHit[fem_ch] = hit_timing;
			if (chHitOld[fem_ch]!=-1 && chHit[fem_ch]<chHitOld[fem_ch])     chHitDiff = chHitOld[fem_ch]-chHit[fem_ch];
			else if(chHitOld[fem_ch]!=-1 && chHit[fem_ch]>chHitOld[fem_ch]) chHitDiff = chHitOld[fem_ch]+(0x1fff-chHit[fem_ch]);		    	
			else ;
		    	if(chHitOld[fem_ch]!=-1){  printf(", chHitDiff: %5d",chHitDiff); tdc_chDiff[fem_id][fem_ch]->Fill(chHitDiff); }
		    	chHitOld[fem_ch] = hit_timing;

			//error flag
		    	if(dc5_tdc_rwOverlap!=0) printf("\nError_ : dc5_tdc_rwOverlap!=0!");
		    	if(dc5_tdc_maxLimt!=0)   printf("\nError_ : dc5_tdc_maxLimt!=0!");

			
		
		     }//...............for(int ihit=femFirst+1; ihit<femEnd; ihit++) 
		
		  }//....else if ( (wordInt[j]>>31)==0 && (wordInt[j]>>24)==0x5a )
	
		else ; //fem_data
		
	      }//...femDataAna....else
	    
	  }//......packageloop.......for(int j=lineFirst; j<=lineEnd; 
	lineFirst = -1; lineEnd = -1; 
	error_fem_evt_num   = false;

      }//.........see package trailer....else if(wordStr[i]=="cfed1200") 
    else ; //do nothing except for 00000 cand cdf...;
  }//.........dataloop...........for(int i=0; i<numWord; i++){
  


  //=========== save histogram in root file ===========
  TFile*f= new TFile(Form("%s/%i_plot.root",dir,thres),"recreate","tdc");
   
  double mean[numFEM][numFEMCh],RMS[numFEM][numFEMCh];
  for(int iFEM=0;iFEM<numFEM;iFEM++)
    { 
      tdc_trig[iFEM]->Write(Form("FEM%02d : trig",iFEM));
      tdc_trigDiff[iFEM]->Write(Form("FEM%02d : trigDiff",iFEM));
      phase_trig[iFEM]->Write(Form("FEM%02d : trigPhase",iFEM));
      
      

      for(int iCh=0; iCh<numFEMCh; iCh++)
	{ 
     
	  mean[iFEM][iCh]=(tdc_ch[iFEM][iCh]->GetMean());
	  RMS[iFEM][iCh]=(tdc_ch[iFEM][iCh]->GetRMS());
     
	  //printf("\n %3d  %3d %10.2f %10.2f", iFEM, iCh, mean[iFEM][iCh], RMS[iFEM][iCh]);
	  tdc_ch[iFEM][iCh]->Write(Form("FEM%02d_Ch%02d : hit",iFEM,iCh));
	  tdc_ch_trig[iFEM][iCh]->Write(Form("FEM%02d_Ch%02d : hit-trig",iFEM,iCh));
	  tdc_chDiff[iFEM][iCh]->Write(Form("FEM%02d_Ch%02d : hit(n)-hit(n+1)",iFEM,iCh));
	  phase_ch[iFEM][iCh]->Write(Form("FEM%02d_Ch%02d : phase",iFEM,iCh));
	}  
    }
  //f->ls();
  //f->Close();

}  
  
